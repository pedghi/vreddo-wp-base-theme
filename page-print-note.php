<style>
    a {
        text-decoration: none;
        color: inherit;
    }
</style>
<?php
$note = get_post($_GET['note_id']);

if (!current_user_can('tutor_admin') && !current_user_can('administrator') && $note->post_author != wp_get_current_user()->ID) {
    echo '<script>window.close();</script>';
    die();
}

$loc = get_post_meta($note->ID, '_nt-course-array', true);
echo nt_course_breadcrumbs($loc);
echo '<br />';
echo $note->post_content;
?>
<script>
    window.print();
    window.close();
</script>