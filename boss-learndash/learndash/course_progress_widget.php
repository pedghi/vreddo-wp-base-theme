<?php
/**
 * Displays the course progress widget.
 *
 * @since 2.1.0
 *
 * @package LearnDash\Course
 */

// Maybe there would be a more global way to do this.
global $post;

$course_progress = rm_learndash_course_progress();
?>

<div class="progress-wrap">
    <span class="course-completion-rate"><?= $course_progress['completed'] ?> out of <?= $course_progress['total'] ?> steps completed</span><span class="percent"><?php echo $course_progress['percentage']; ?>%</span>

    <dd class="course_progress" title="<?php echo sprintf( __( '%s out of %s steps completed', 'boss-learndash' ), $course_progress['completed'], $course_progress['total'] ); ?>">
		<div class="course_progress_blue" style="width: <?php echo esc_attr( $course_progress['percentage'] ); ?>%;"></div>
    </dd>
</div>