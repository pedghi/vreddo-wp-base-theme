<?php
include __DIR__ . '/template/page-start.php';

if (!is_user_logged_in() || (!current_user_can('administrator') && !current_user_can('tutor_admin'))) {
    auth_redirect();
}

$user = (isset($_GET['user']) && (current_user_can('tutor_admin') || current_user_can('administrator'))) ? get_user_by('login', $_GET['user']) : wp_get_current_user();

$atts = apply_filters( 'bp_learndash_user_courses_atts', array());
$userCourses = apply_filters( 'bp_learndash_user_courses', ld_get_mycourses($user->ID,  $atts));
?>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(function() {
        $(".hierarchy").each(function() {
            $(this).sortable({
                connectWith: [".hierarchy-" + $(this).data("id"), ".graveyard-" + $(this).data("id")]
            }).disableSelection();
        });

        $(".lesson-children").each(function() {
            $(this).sortable({
                connectWith: [".lesson-children-" + $(this).data("course-id"), ".topic-graveyard-" + $(this).data("course-id")]
            }).disableSelection();
        });

        $(".graveyard").each(function() {
            $(this).sortable({
                connectWith: [".hierarchy-" + $(this).data("id")]
            }).disableSelection();
        });
        
        $(".topic-graveyard").each(function() {
            $(this).sortable({
                connectWith: [".lesson-children-" + $(this).data("id")]
            }).disableSelection();
        });
    });
</script>
<style>
    .lesson {
        padding: 8px;
        border: 1px solid black;
        color: black;
        background-color: rgb(235, 245, 251);
        font-size: 11px;
        margin-bottom: 10px;
    }

     .lesson-action {
        float: right;
        background-color: rgb(184, 184, 184);
        padding: 0px 4px;
        margin-left: 6px;
        color: white;
    }
    
    .lesson-child {
        padding: 8px;
        font-size: 10px;
        margin-top: 6px;
        border: 1px solid black;
    }
    
    .lesson-child.sfwd-topic {
        background-color: rgb(253, 237, 236);
    }
    
    .lesson-child.sfwd-quiz {
        background-color: rgb(252, 243, 207);
    }

    .graveyard:empty {
        padding: 20px;
        border: 1px dashed black;
        font-size: 11px;
    }

    .graveyard:empty:before {
        content: "Drop lessons here to remove them from the course flow.";
    }

    .topic-graveyard:empty {
        padding: 20px;
        border: 1px dashed black;
        font-size: 11px;
    }

    .topic-graveyard:empty:before {
        content: "Drop topics/quizzes here to remove them from the course flow.";
    }
</style>
<?php
function renderLesson($course, $lesson) {
    ?>
    <div class="lesson" data-id="<?= $lesson['lesson']->ID ?>">
        <span class="float: left;">
            <?= $lesson['lesson']->post_title ?>
        </span>
        <div class="lesson-action" data-action="expand">
            <i class="fa fa-chevron-down"></i>
        </div>
        <!--
        <div class="lesson-action">
            <i class="fa fa-times"></i>
        </div>
        !-->
        <div class="lesson-children lesson-children-<?= $course->ID ?>" data-course-id="<?= $course->ID ?>" style="display: none; overflow: auto;">
            <?php
            foreach ($lesson['children'] as $child) {
                renderTopic($course, $child);
            }
            ?>
        </div>
    </div>
    <?php
}

function renderTopic($course, $topic) {
    ?>
    <div class="lesson-child <?= $topic->post_type ?>" data-course-id="<?= $course->ID ?>" data-id="<?= $topic->ID ?>"><?= $topic->post_title ?></div>
    <?php
}

foreach ($userCourses as $courseId) {
    $course = get_post($courseId);
    $hierarchy = rm_get_course_steps_hierarchy($course->ID, $user->ID);
    $originalHierarchy = rm_get_course_steps_hierarchy($course->ID);
    $lessonGraveyard = array();
    $topicGraveyard = array();

    // Check which lessons have been removed.
    foreach ($originalHierarchy as $lesson) {
        if (count(array_filter($hierarchy, function($item) use ($lesson) { return $item['lesson']->ID == $lesson['lesson']->ID; })) == 0) {
            // Check its children and make sure its children are not present in other, non-deleted lessons.
            $lesson['children'] = array_filter($lesson['children'], function ($child) use ($hierarchy) {
                foreach ($hierarchy as $usedLesson) {
                    foreach ($usedLesson['children'] as $usedTopic) {
                        if ($usedTopic->ID == $child->ID) {
                            return false;
                        }
                    }
                }

                return true;
            });
            
            // Place the remaining lesson in the graveyard.
            $lessonGraveyard[] = $lesson;
        }
    }
    
    // Check which topics have been removed.
    foreach ($originalHierarchy as $lesson) {
        $removedChildren = array_filter($lesson['children'], function($child) use ($hierarchy, $lessonGraveyard) {
            foreach (array_merge($hierarchy, $lessonGraveyard) as $usedLesson) {
                foreach ($usedLesson['children'] as $usedTopic) {
                    if ($usedTopic->ID == $child->ID) {
                        return false;
                    }
                }
            }

            return true;
        });

        foreach ($removedChildren as $child) {
            $topicGraveyard[] = $child;
        }
    }
?>
    <form method="POST">
        <h2><?= $course->post_title ?></h2>
        <div class="graveyard graveyard-<?= $course->ID ?>" data-id="<?= $course->ID ?>" style="overflow: auto;"><?php
        foreach ($lessonGraveyard as $lesson) {
            renderLesson($course, $lesson);
        }
        ?></div>
        <br />
        <div class="topic-graveyard topic-graveyard-<?= $course->ID ?>" data-id="<?= $course->ID ?>" style="overflow: auto;"><?php
        foreach ($topicGraveyard as $topic) {
            renderTopic($course, $topic);
        }
        ?></div>
        <h3>Course Flow</h3>
        <div class="hierarchy hierarchy-<?= $course->ID ?>" data-id="<?= $course->ID ?>" style="overflow: auto;">
        <?php
        foreach ($hierarchy as $lesson) {
            renderLesson($course, $lesson);
        }
        ?>
        </div>
        <button type="button" class="button"
        data-ajax-click="saveCourseFlow"
        data-ajax-arg-course-id="<?= $course->ID ?>"
        data-ajax-arg-user-id="<?= $user->ID ?>"
        data-ajax-arg-course-flow="js:getHierarchyData(<?= $course->ID ?>)"
        data-ajax-success="alert('Saved.')"
        >Save</button>
    </form>
<?php
}
?>
<?php
include __DIR__ . '/template/page-end.php';
?>