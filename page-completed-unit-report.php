<?php
include __DIR__ . '/template/page-start.php';

if (!is_user_logged_in() || (!current_user_can('administrator') && !current_user_can('tutor_admin'))) {
    auth_redirect();
}

$user = (isset($_GET['user']) && (current_user_can('tutor_admin') || current_user_can('administrator'))) ? get_user_by('login', $_GET['user']) : wp_get_current_user();

$statMapper = new RedMako_WpProQuiz_Model_StatisticRefMapper();
$quizMapper = new WpProQuiz_Model_QuizMapper();
$quizAttempts = $statMapper->getUserQuizAttempts($user->ID);

$courses = [];
$lessons = [];
$units = [];
$structure = [];

foreach ($quizAttempts as $attempt) {
    if ($attempt->getIncorrectCount() == 0) {
        $quizId = learndash_get_quiz_id_by_pro_quiz_id($attempt->getQuizId());
        $course = rm_get_course_for_quiz($quizId);
        
        if ($course) {
            if (!isset($courses[$course->ID])) {
                $courses[$course->ID] = $course;
                $structure[$course->ID] = [];
            }
            
            $lesson = learndash_get_lesson_id($quizId, $course->ID);

            if ($lesson) {
                $lesson = get_post($lesson);

                if (!isset($lessons[$lesson->ID])) {
                    $lessons[$lesson->ID] = $lesson;
                    $structure[$course->ID][$lesson->ID] = [];
                }

                $structure[$course->ID][$lesson->ID][] = $attempt;
            }
        }
    }
}
?>
<style>
    td.detail {
        padding-left: 20px;
        font-size: 14px !important;
    }
</style>
<table>
    <?php
    foreach ($structure as $cid => $courseLessons) {
        $course = $courses[$cid];
    ?>
    <tr>
        <th><a href="<?= get_the_permalink($course) ?>"><?= $course->post_title ?></a></th>
    </tr>
    <?php
        foreach ($courseLessons as $lid => $quizzes) {
            $lesson = $lessons[$lid];

            ?>
    <tr>
        <td><a href="<?= get_the_permalink($lesson) ?>"><?= $lesson->post_title ?></a></td>
    </tr>
            <?php
            foreach ($quizzes as $quizAttempt) {
                $quiz = $quizMapper->fetch($quizAttempt->getQuizId());
                
                ?>
    <tr>
        <td class="detail"><a href="<?= get_the_permalink(learndash_get_quiz_id_by_pro_quiz_id($quiz->getId())) ?>"><?= $quiz->getName() ?></a></td>
    </tr>
                <?php
            }
        }
    }
    ?>
</table>
<?php
include __DIR__ . '/template/page-end.php';
?>