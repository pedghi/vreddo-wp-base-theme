<?php
function rm_quiz_pro_id_from_post_id($quiz_post_id) {
  $id = get_post_meta($quiz_post_id, 'quiz_pro_id', true);
  return $id;
}

function array_unique_callback(array $arr, callable $callback, $strict = false) {
	return array_filter(
		$arr,
		function ($item) use ($strict, $callback) {
			static $haystack = array();
			$needle = $callback($item);
			if (in_array($needle, $haystack, $strict)) {
				return false;
			} else {
				$haystack[] = $needle;
				return true;
			}
		}
	);
}

function rm_learndash_lesson_progress($course_id, $lesson, $user_id = null)
{
  $stat_mapper = new RedMako_WpProQuiz_Model_StatisticRefMapper();

	if ( empty( $user_id ) ) {
		$current_user = wp_get_current_user();
		$user_id = $current_user->ID;
  }
  
  $course_progress = rm_get_user_course_progress($user_id, $course_id);

  // Will set this to false if we find a quiz that isn't 100%.
  $has_quiz = false;
  $lesson_complete = false;
  
  $progress = array();
  
  foreach ($lesson['children'] as $child) {
    if ($child->post_type == 'sfwd-quiz') {
      if (!$has_quiz) {
        $has_quiz = true;
        $lesson_complete = true;
      }

      $attempt = $stat_mapper->getUserQuizAttempt($user_id, rm_quiz_pro_id_from_post_id($child->ID));
      
      if (!$attempt || $attempt->getPoints() != $attempt->getGPoints()) {
        $lesson_complete = false;
        $progress[$child->ID] = false;
      } else {
        $progress[$child->ID] = true;
      }
    }
  }

  foreach ($lesson['children'] as $child) {
    if ($child->post_type == 'sfwd-topic') {
      if ($lesson_complete) {
        $progress[$child->ID] = true;
      } else {
        $progress[$child->ID] = $course_progress['topics'][$lesson['lesson']->ID][$child->ID];
      }
    }
  }

  return $progress;
}

function rm_quiz_lesson_complete($quiz_id, $user_id) {
	$lesson = rm_get_lesson_for_quiz($quiz_id, $user_id);
	$progress = rm_learndash_lesson_progress(rm_get_course_for_quiz($quiz_id)->ID, $lesson, $user_id);

	foreach ($progress as $item) {
		if (!$item) {
			return false;
		}
	}
	
	return true;
}

function rm_get_user_course_progress($user_id, $course_id = null) {
  $course_progress = get_user_meta($user_id, '_sfwd-course_progress', true);

  if (empty($course_id)) {
    return $course_progress;
  }
  
  $course_progress = isset($course_progress[$course_id]) ? $course_progress[$course_id] : array();
  return $course_progress;
}

function rm_learndash_course_progress($options = array())
{
  extract( shortcode_atts( array( 'course_id' => 0, 'user_id' => 0, 'array' => false, ), $options ) );

	if ( empty( $user_id ) ) {
		$current_user = wp_get_current_user();
		$user_id = $current_user->ID;
	}

	if ( empty( $course_id ) ) {
		$course_id = learndash_get_course_id();
  }
  
  $course_flow = rm_get_course_steps_hierarchy($course_id, $user_id);
  $course_progress = rm_get_user_course_progress($user_id, $course_id);
  $total = 0;
  $completed = 0;
  
  // Count the total amount of lessons, topics, and quizzes, and check their completion.
  foreach ($course_flow as $lesson) {
    $progress = rm_learndash_lesson_progress($course_id, $lesson, $user_id);
    $total += count($progress) + 1;
    $children_completed = 0;
    $children_incomplete = 0;

    foreach ($progress as $child => $passed) {
      if ($passed) {
        $children_completed++;
      } else {
        $children_incomplete++;
      }
    }

    $completed += $children_completed;

    if ($children_incomplete == 0) {
      $completed++;
    }
  }

  if ($total == 0) {
    return array(
      'percentage' => 0,
      'completed' => 0,
      'total' => 0
    );
  }

  return array(
    'percentage' => floor($completed / $total * 100),
    'completed' => $completed,
    'total' => $total
  );
  
  return learndash_course_progress($options);
}

function rm_get_lesson_for_quiz($quiz_id, $user_id = null) {
	$course = rm_get_course_for_quiz($quiz_id);
	$steps = rm_get_course_steps_hierarchy($course->ID, $user_id);

	foreach ($steps as $lesson) {
		foreach ($lesson['children'] as $child) {
			if ($child->ID == $quiz_id) {
				return $lesson;
			}
		}
	}

	return null;
}

function rm_get_course_for_quiz($quiz_id)
{
  $courses   = learndash_get_courses_for_step($quiz_id);
  $primaries   = array_keys($courses['primary']);
  $secondaries = array_keys($courses['secondary']);
  $id      = null;
  
  if (count($primaries) > 0) {
    $id = $primaries[0];
  }
  
  if (count($secondaries) > 0) {
    $id = $secondaries[0];
  }
  
  if ($id) {
    return get_post($id);
  }
  
  return null;
}

function rm_get_max_quiz_attempts($course_id, $quiz_id = null)
{
  if ($quiz_id) {
    $quiz_attempts = get_field('max_quiz_attempts', $quiz_id);
    
    if ($quiz_attempts) {
      return $quiz_attempts;
    }
  }
  
  $quiz_attempts = get_field('max_quiz_attempts', $course_id);
  
  if ($quiz_attempts) {
    return $quiz_attempts;
  }
  
  return null;
}

function rm_get_custom_course_flow($course_id, $user_id)
{
  return get_post_meta($course_id, '_custom_course_flow_' . $user_id, true);
}

function rm_get_course_steps_hierarchy($course_id = null, $user_id = null)
{
	if ( empty( $course_id ) ) {
		$course_id = learndash_get_course_id();
  }
  
  $steps = get_post_meta($course_id, 'ld_course_steps', true);
  
  $custom_steps = null;
  
  if ($user_id) {
    $custom_steps = get_post_meta($course_id, '_custom_course_flow_' . $user_id, true);
  }
  
  if ($custom_steps) {
    $lessons = array();
    
    foreach ($custom_steps as $step) {
      $lesson = array(
        'lesson' => get_post($step['id']),
        'children' => array()
      );
      
      foreach ($step['children'] as $child) {
        $lesson['children'][] = get_post($child);
      }
      
      $lessons[] = $lesson;
    }
    
    return $lessons;
  }
  
  if (!isset($steps['h'])) {
    return array();
  }
  
  $steps   = $steps['h'];
  $lessons = array();
  
  foreach ($steps['sfwd-lessons'] as $id => $lesson) {
    $children = array();
    
    foreach ($lesson['sfwd-topic'] as $cid => $child) {
      $children[] = get_post($cid);
    }
    
    foreach ($lesson['sfwd-quiz'] as $cid => $child) {
      $children[] = get_post($cid);
    }
    
    $lessons[] = array(
      'lesson' => get_post($id),
      'children' => $children
    );
  }
  
  return $lessons;
}

function rm_bp_displayed_user_name()
{
  return get_userdata(bp_displayed_user_id())->user_login;
}

function rm_format_duration($duration) {
  $d = $duration;
  $f = [];
  
  // Hours
  if ($d > 3600) {
    $v = floor($d / 3600);
    $f[] = $v . 'h';
    $d -= $v * 3600;
  }

  // Mins
  if ($d > 60) {
    $v = floor($d / 60);
    $f[] = $v . 'm';
    $d -= $v * 60;
  }

  // Seconds
  if ($d > 0) {
    $v = floor($d);
    $f[] = $v . 's';
    $d -= $v;
  }

  if (count($f) == 0) {
    return '0s';
  }

  return implode(' ', $f);
}

function rm_sugarcrm_client() {
	$client = new RedMako_SugarCrm_Client(get_option('sugarcrm_endpoint_url'));
	$client->authenticate(get_option('sugarcrm_username'), get_option('sugarcrm_password'));
	return $client;
}

function rm_sugarcrm_push_unit_completion($user_id, $unit_number) {
	$client = rm_sugarcrm_client();
	$trainee_id = get_user_meta($user_id, 'sugarcrm_id', true);
	
	if ($trainee_id) {
		$trainee = $client->get_trainee(get_user_meta($user_id, 'sugarcrm_id', true));
		$training_plan = $client->get_training_plan_for_trainee($trainee->id);
		$unit = $client->get_unit_for_training_plan_by_name($training_plan->id, $unit_number);

		$update = array(
			'unit_quiz_assessment' => true
		);

		if ($unit->unit_observation_assessment == 'Approved' && $unit->unit_3rd_party == 'Approved') {
			$update['completed_assessment_date'] = date('Y-m-d');
		}

		$client->update_unit($training_plan->id, $unit->id, $update);
		
		return true;
	}
}

add_action('wp_ajax_wp_pro_quiz_completed_quiz', function() {
	register_shutdown_function(function() {
		$post = get_post($_POST['lesson_id']);
		$unit = explode(' ', $post->post_title)[0];
		rm_sugarcrm_push_unit_completion(wp_get_current_user()->ID, $unit);
	});
}, 1);

function rm_exclude_menu_items( $items, $menu, $args ) {
	$user_roles = wp_get_current_user()->roles;
	$hide = false;

	foreach ($user_roles as $role) {
		if ($role == 'subscriber' || $role == 'participant') {
			$hide = true;
		}
	}

	if ($hide) {
		foreach ( $items as $key => $item ) {
			if ( $item->title == 'Tutor Dashboard' ) unset( $items[$key] );
		}
	}

	return $items;
}

add_filter( 'wp_get_nav_menu_items', 'rm_exclude_menu_items', null, 3 );

/*
	* Get user classes. Used initially by boss-learndash/learndash templates
	*/
	function get_user_classes($user_id, $course_id){
		
		//$classes = get_field('course_class',  $course_id);

		$args = array(
			'post_type' => 'course-class',
			'meta_query'=> array(
				'relation' => 'AND',
				array(
					'key' => 'course_class',
					'value' => sprintf(':"%s";', $course_id),
					'compare' => 'LIKE'
				),
				array(
					'key' => 'class_students',
					'value' => sprintf(':"%s";', $user_id),
					'compare' => 'LIKE'
				),
			)
		);
		$classes = new WP_Query($args);
		
		// TODO: remove nested loops:
		$user_classes = array();
		if ( $classes ) {
			foreach ($classes->posts as $key => $class) {
				$students = get_field('class_students', $class->ID);
				foreach ($students as $key => $student) {
					if ($user_id == $student['ID']) {
						$user_classes[] = $class;	
					}
				}
			}
		}
		return $user_classes;
	}

	/*
	* Filter students field only by users enrolled on the Course related to Class
	*/
	function class_students_filter( $args, $field, $post_id ) {
		$course = get_field('course_class', $post_id);
		$course_id = $course[0]->ID;
		
		$course_users_query = learndash_get_users_for_course($course_id, array(), false);
		$user_ids = $course_users_query->get_results();
		
		$args['include'] = $user_ids;
		
		// return
		return $args;
	}
	
	
	// filter for every field
	//add_filter('acf/fields/relationship/query', 'my_relationship_query', 10, 3);
	
	// filter for a specific field based on it's name
	add_filter('acf/fields/user/query/name=class_students', 'class_students_filter', 10, 3);
	
	// filter for a specific field based on it's key
	//add_filter('acf/fields/relationship/query/key=field_508a263b40457', 'my_relationship_query', 10, 3);

	/*
	* Adds Class Content shortcode
	*/
	function class_content_shortcode( $atts ,  $content = "") {
		// get shortcode class
		$class_id = $atts['course_class'];
		$args = array('p' => $class_id, 'post_type' => 'course-class');
		$classes = new WP_Query($args);
		$classes = $classes->posts;
		$class = $classes[0];

		// get current user
		$user = wp_get_current_user();

		// get Class Students ID list
		$class_users = get_field('class_students', $class_id);
		foreach ($class_users as $class_user) {
			$class_user_ids[] = $class_user['ID'];
		}
		
		// verifies if user is in class
		if (in_array($user->ID, $class_user_ids))
			return "{$content}";
		else return null;
	}
	add_shortcode( 'class_content', 'class_content_shortcode' );
	
	
	// add_action( 'admin_init', function() {
	// 	if ( $role = get_role( 'administrator' ) ) {
	// 		$role->add_cap( 'manage_admin_columns' );
	// 	}
	// 	if ( $role = get_role( 'tutor_admin' ) ) {
	// 		$role->add_cap( 'manage_admin_columns' );
	// 	}
	// } );

	/*
	* Add class information to User profile
	*/
	add_action( 'show_user_profile', 'extra_user_profile_fields' );
	add_action( 'edit_user_profile', 'extra_user_profile_fields' );

	function extra_user_profile_fields( $user ) { ?>
		<h3><?php _e("Course Classes", "blank"); ?></h3>
		<?php 
			$args = array(
				'post_type' => 'course-class',
				'meta_query'=> array(
					array(
						'key' => 'class_students',
						'value' => sprintf(':"%s";', $user->ID),
						'compare' => 'LIKE'
					),
				)
			);
			$classes = new WP_Query($args);

			if ($classes->posts) {
				foreach ($classes->posts as $key => $class) {
					
					
					echo get_field('class_public_title', $class->ID) . ' | ' . get_field('class_datetime', $class->ID);;
					if (end($classes) != $class ) echo '<br/>';
				}
			}
		
	} 
	
	// create Classes options menu
	add_action('admin_menu', 'rm_class_options_create_menu');

	function rm_class_options_create_menu() {

		//create new top-level menu
		//add_menu_page('My Cool Plugin Settings', 'Cool Settings', 'administrator', __FILE__, 'rm_class_options_settings_page' , plugins_url('/images/icon.png', __FILE__) );

		add_submenu_page(
			'edit.php?post_type=course-class',
			__( 'Classes Overview', 'textdomain' ),
			__( 'Overview', 'textdomain' ),
			'manage_options',
			'class-options',
			'rm_class_options_settings_page'
		);
		
		//call register settings function
		add_action( 'admin_init', 'register_rm_class_options_settings' );
	}


	function register_rm_class_options_settings() {
		//register our settings
		register_setting( 'rm-class-settings-group', 'new_option_name' );
		register_setting( 'rm-class-settings-group', 'some_other_option' );
		register_setting( 'rm-class-settings-group', 'option_etc' );
	}

	function rm_class_options_settings_page() {
	?>
		<div class="wrap">
		<h1>Class Overview</h1>

		<form method="post" action="options.php">
			<?php settings_fields( 'rm-class-settings-group' ); ?>
			<?php do_settings_sections( 'rm-class-settings-group' ); ?>
			<table class="form-table">
				<tr valign="top">
				<th scope="row">New Option Name</th>
				<td><input type="text" name="new_option_name" value="<?php echo esc_attr( get_option('new_option_name') ); ?>" /></td>
				</tr>
				
				<tr valign="top">
				<th scope="row">Some Other Option</th>
				<td><input type="text" name="some_other_option" value="<?php echo esc_attr( get_option('some_other_option') ); ?>" /></td>
				</tr>
				
				<tr valign="top">
				<th scope="row">Options, Etc.</th>
				<td><input type="text" name="option_etc" value="<?php echo esc_attr( get_option('option_etc') ); ?>" /></td>
				</tr>
			</table>
			
			<?php submit_button(); ?>

		</form>
		</div>
		
		<div>
			<table class="course-classes-list wp-list-table widefat fixed striped media">
				<thead>
					<th class="manage-column column-title column-primary">Class</th>
					<th class="manage-column">Status</th>
					<th class="manage-column">Date/Time</th>
					<th class="manage-column">Course</th>
				</thead>
				<tbody>
					<?php 
						$classes = get_course_classes_details(); 
						if ( $classes ) :
							foreach ($classes as $class) { ?>
								<tr class="">
									<td>
										<a href="/wp-admin/post.php?post=<?=$class['ID']?>&action=edit"><?= $class['title']?></a> (<?= $class['public_title']?>)
									</td>	
									<td><?= $class['status']?></td>
								
									<td>
									
										<?= DateTime::createFromFormat('d\/m\/Y g:i a', $class['date_time'])->format('d/m/Y H:i (l) ');?></td>
									</td>
								
									<td>
										<a href="/wp-admin/post.php?post=<?= $class['course_id']?>&action=edit"><?= $class['course_title']?></a>										
									</td>
								</tr>
							<?php }?>		
					<?php endif; ?>
				</tbody>
			</table>

			
				
				
		</div>
		
	<?php }	

	function get_course_classes_details ($course_id = 0, $user_id = 0) {
		$args = array(
			'post_type' => 'course-class',
			'meta_key' => 'class_datetime',
			'orderby' => array (
				'meta_value' => 'ASC',
			)
		);
		$classes = new WP_Query($args);

		if ($classes->posts) {
			foreach ($classes->posts as $key => $class) {
				$course = get_field('course_class', $class->ID);

				$classes_list[$key] = array(
					'ID' => $class->ID,
					'title' => get_the_title($class->ID),
					'status' => get_post_status($class->ID), 
					'public_title' => get_field('class_public_title', $class->ID),
					'date_time' => get_field('class_datetime', $class->ID),
					'course_id' => $course[0]->ID,
					'course_title' => $course[0]->post_title,

				);
			}
		}
		return $classes_list;
	}

	/**
	 * Add Course - VR Content meta box
	 *
	 * @param post $post The post object
	 * @link https://codex.wordpress.org/Plugin_API/Action_Reference/add_meta_boxes
	 */
	function course_add_meta_boxes( $post ){
		add_meta_box( 'vr_content_meta_box', 'VR Content', 'vr_content_build_meta_box', 'sfwd-courses' );
	}
	add_action( 'add_meta_boxes_sfwd-courses', 'course_add_meta_boxes' );

	/**
	 * Build custom field meta box
	 *
	 * @param post $post The post object
	 */
	function vr_content_build_meta_box( $post ){
		$class_args = array(
			'post_type' 	=> 'course-class',
			'fields'		=> 'ids',
			'meta_query'	=> array(
				'relation' => 'AND',
					array(
						'key' => 'course_class',
						'value' => sprintf(':"%s";', $post->ID),
						'compare' => 'LIKE'
					),
			)
		);
		$classes = new WP_Query($class_args);
		$classes_ids = $classes->posts;
		if ($classes) {
			foreach ($classes_ids as $class_id) {
				$vr_content_class_num[$class_id] = (int) get_post_meta($class_id, 'vr_content', ARRAY_A);
			}
			?>
			<h4>Classes VR Content</h4>
			<table class="course-classes-vr-content-list wp-list-table widefat fixed striped media">
					<thead>
						<th class="manage-column column-title column-primary">Class</th>
						<th class="manage-column" width=10%>qty</th>
						<th class="manage-column">VR Content</th>
					</thead>
					<tbody>
						<?php foreach ($classes_ids as $class_id) : ?>
							<?php if ($vr_content_class_num[$class_id]) : ?>	
								<tr class="">
									<td>
										<a href="/wp-admin/post.php?post=<?=$class_id?>&action=edit"><?= get_the_title($class_id)?></a> (<?= get_post_meta($class_id, 'class_public_title', ARRAY_A)?>)
									</td>
									<td>
									<?= $vr_content_class_num[$class_id]?>
									</td>	
									<td>
										<?php
										for ($i=0; $i < $vr_content_class_num[$class_id] ; $i++) { 
											$vr_content_id = get_post_meta($class_id, 'vr_content_'.$i.'_vr_content_file', ARRAY_A);
											echo wp_get_attachment_image( $vr_content_id, array(30,30), '', array( "title" => basename(get_attached_file($vr_content_id))));
										}
										?>
									</td>
								</tr>
							<?php endif; ?>
						<?php endforeach;?>		
					</tbody>
				</table>
			<?php
		}

		$lessons = learndash_get_course_lessons_list( $post->ID );

		if ($lessons) { 
			foreach ($lessons as $lesson) {
				$lesson_id = $lesson['post']->ID;
				$vr_content_lesson_num[$lesson_id] = (int) get_post_meta($lesson_id, 'vr_content', ARRAY_A);
				$lesson_topics[$lesson_id] = learndash_get_topic_list( $lesson_id, $post->ID );
			}
			foreach ($lesson_topics as $lesson_key => $lesson_topics) {
				foreach ($lesson_topics as $topic) {
					$vr_content_topic_num = get_post_meta($topic->ID, 'vr_content');
					if ( $vr_content_topic_num[0] > 0 ) {
						$vr_content_topic_number[$lesson_key][$topic->ID] = $vr_content_topic_num[0];
					}
				}
			}
			?>
			<h4>Lessons VR Content</h4>
			<table class="course-lessons-vr-content-list wp-list-table widefat fixed striped media">
					<thead>
						<th class="manage-column column-title column-primary">Lesson</th>
						<th class="manage-column" width=10%>qty</th>
						<th class="manage-column">VR Content</th>
					</thead>
					<tbody>
						<?php foreach ($lessons as $lesson) : ?>
							<?php 
							$lesson_id = $lesson['post']->ID;
							if ($vr_content_lesson_num[$lesson_id] || $vr_content_topic_number[$lesson_id]) : ?>	
								<tr class="">
									<td>
										<a href="/wp-admin/post.php?post=<?=$lesson_id?>&action=edit"><?= get_the_title($lesson_id)?></a>
										
									</td>
									<td>
										<?= $vr_content_lesson_num[$lesson_id]?>
									</td>
									<td>
										<?php
										if ($vr_content_lesson_num[$lesson_id]){
											for ($i=0; $i < $vr_content_lesson_num[$lesson_id] ; $i++) { 
												$vr_content_id = get_post_meta($lesson_id, 'vr_content_'.$i.'_vr_content_file', ARRAY_A);
												if (strpos(get_post_mime_type( $vr_content_id ), 'video') !== false) : ?>
													<img src="/wp-includes/images/media/video.png" title="<?= basename(get_attached_file($vr_content_id)); ?>" width=30 height=30/>
												<?php elseif (strpos(get_post_mime_type( $vr_content_id ), 'image') !== false) : ?>
													<?= wp_get_attachment_image( $vr_content_id, array(30,30), '', array( "title" => basename(get_attached_file($vr_content_id)))); ?>
												<?php else : ?>
													<img src="/wp-includes/images/media/default.png" title="<?= basename(get_attached_file($vr_content_id)); ?>" width=30 height=30/>
												<?php endif;
												
											}
										}
										else {
											echo '---';
										}	
										?>
									</td>
								</tr>
								<?php foreach ($vr_content_topic_number[$lesson_id] as $topic_id => $topic_vr_content) : ?>
									<tr class="">
										<td>
											&nbsp;&nbsp;&nbsp;topic: <a href="/wp-admin/post.php?post=<?=$topic_id?>&action=edit"><?= get_the_title($topic_id)?></a>
										</td>
										<td>
											<?= $vr_content_topic_number[$lesson_id][$topic_id]?>
										</td>
										<td>
											<?php
											for ($i=0; $i < $vr_content_topic_number[$lesson_id][$topic_id] ; $i++) { 
												$vr_content_id = get_post_meta($topic_id, 'vr_content_'.$i.'_vr_content_file', ARRAY_A);
												if (strpos(get_post_mime_type( $vr_content_id ), 'video') !== false) : ?>
													<img src="/wp-includes/images/media/video.png" title="<?= basename(get_attached_file($vr_content_id)); ?>" width=30 height=30/>
												<?php elseif (strpos(get_post_mime_type( $vr_content_id ), 'image') !== false) : ?>
													<?= wp_get_attachment_image( $vr_content_id, array(30,30), '', array( "title" => basename(get_attached_file($vr_content_id)))); ?>
												<?php else : ?>
													<img src="/wp-includes/images/media/default.png" title="<?= basename(get_attached_file($vr_content_id)); ?>" width=30 height=30/>
												<?php endif;
											}
											?>
										</td>
									</tr>
								<?php endforeach;?>
							<?php endif; ?>
						<?php endforeach;?>		
					</tbody>
				</table>
			<?php

		}
		
	}
?>