<?php
add_action('admin_menu', 'rm_admin_menu');

function rm_admin_menu() {
    add_menu_page('SugarCRM Settings', 'SugarCRM Settings', 'administrator', 'sugarcrm-settings', 'rm_sugarcrm_settings_page');
    add_action('admin_init', 'rm_sugarcrm_settings');
}

function rm_sugarcrm_settings() {
    register_setting('sugarcrm-settings-group', 'sugarcrm_endpoint_url');
    register_setting('sugarcrm-settings-group', 'sugarcrm_username');
    register_setting('sugarcrm-settings-group', 'sugarcrm_password');
}

function rm_sugarcrm_settings_page() {
    ?>
    <div class="wrap">
        <h1>SugarCRM Settings</h1>
        <form method="POST" action="options.php">
            <?php settings_fields('sugarcrm-settings-group'); ?>
            <?php do_settings_sections('sugarcrm-settings-group'); ?>
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">Endpoint URL</th>
                    <td><input type="text" name="sugarcrm_endpoint_url" value="<?= esc_attr(get_option('sugarcrm_endpoint_url')) ?>" /></td>
                </tr>
                <tr valign="top">
                    <th scope="row">Username</th>
                    <td><input type="text" name="sugarcrm_username" value="<?= esc_attr(get_option('sugarcrm_username')) ?>" /></td>
                </tr>
                <tr valign="top">
                    <th scope="row">Password</th>
                    <td><input type="password" name="sugarcrm_password" value="<?= esc_attr(get_option('sugarcrm_password')) ?>" /></td>
                </tr>
            </table>
            <?php submit_button(); ?>
        </form>
    </div>
    <?php
}

add_action( 'show_user_profile', 'rm_user_profile_fields' );
add_action( 'edit_user_profile', 'rm_user_profile_fields' );

function rm_user_profile_fields($user) {
    ?>
    <h3>SugarCRM Integration</h3>

    <table class="form-table">
        <tr>
            <th><label for="sugarcrm_id"><?php _e("SugarCRM ID"); ?></label></th>
            <td>
                <input type="text" name="sugarcrm_id" id="sugarcrm_id" value="<?php echo esc_attr( get_the_author_meta( 'sugarcrm_id', $user->ID ) ); ?>" class="regular-text" />
            </td>
        </tr>
    </table>
<?php
}

add_action( 'personal_options_update', 'rm_save_user_profile_fields' );
add_action( 'edit_user_profile_update', 'rm_save_user_profile_fields' );

function rm_save_user_profile_fields( $user_id ) {
    if ( !current_user_can( 'edit_user', $user_id ) ) { 
        return false; 
    }
    update_user_meta( $user_id, 'sugarcrm_id', $_POST['sugarcrm_id'] );
}