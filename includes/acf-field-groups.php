<?php
if (function_exists('register_field_group')) {
	register_field_group(array(
		'key' => 'redmako_sfwd_courses',
		'title' => 'RedMako Course Settings',
		'fields' => array(
			array(
				'key' => 'field_1',
				'label' => 'Max Quiz Attempts',
				'name' => 'max_quiz_attempts',
				'type' => 'number',
				'instructions' => 'The maximum amount of attempts for quizzes within this course. Leave blank for uncapped.'
			)
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'sfwd-courses'
				),
			)
		)
	));

	register_field_group(array(
		'key' => 'redmako_sfwd_courses',
		'title' => 'RedMako Course Settings',
		'fields' => array(
			array(
				'key' => 'field_2',
				'label' => 'Max Quiz Attempts',
				'name' => 'max_quiz_attempts',
				'type' => 'number',
				'instructions' => 'The maximum amount of attempts for this quiz. If a maximum is set for the course, this value will be used instead. Leave blank for uncapped.'
			)
		),
		'location' => array(
			array(
				array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'sfwd-quiz'
				),
			)
		)
	));
	
}

/*
* Register Course Class Custom Post Type
*/
if ( ! function_exists('rm_courses_class_cpt') ) {

	// Register Custom Post Type
	function rm_courses_class_cpt() {
	
		$labels = array(
			'name'                  => _x( 'Classes', 'Post Type General Name', 'text_domain' ),
			'singular_name'         => _x( 'Class', 'Post Type Singular Name', 'text_domain' ),
			'menu_name'             => __( 'Classes', 'text_domain' ),
			'name_admin_bar'        => __( 'Classes', 'text_domain' ),
			'archives'              => __( 'Item Archives', 'text_domain' ),
			'attributes'            => __( 'Item Attributes', 'text_domain' ),
			'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
			'all_items'             => __( 'All Items', 'text_domain' ),
			'add_new_item'          => __( 'Add New Item', 'text_domain' ),
			'add_new'               => __( 'Add New', 'text_domain' ),
			'new_item'              => __( 'New Item', 'text_domain' ),
			'edit_item'             => __( 'Edit Item', 'text_domain' ),
			'update_item'           => __( 'Update Item', 'text_domain' ),
			'view_item'             => __( 'View Item', 'text_domain' ),
			'view_items'            => __( 'View Items', 'text_domain' ),
			'search_items'          => __( 'Search Item', 'text_domain' ),
			'not_found'             => __( 'Not found', 'text_domain' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
			'featured_image'        => __( 'Featured Image', 'text_domain' ),
			'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
			'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
			'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
			'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
			'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
			'items_list'            => __( 'Items list', 'text_domain' ),
			'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
			'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
		);
		$args = array(
			'label'                 => __( 'Class', 'text_domain' ),
			'description'           => __( 'Courses Classes', 'text_domain' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
			'hierarchical'          => true,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 250,
			'show_in_admin_bar'     => false,
			'show_in_nav_menus'     => false,
			'can_export'            => true,
			'has_archive'           => false,
			'exclude_from_search'   => true,
			'publicly_queryable'    => true,
			'capability_type'       => 'page',
			'show_in_rest'          => false,
		);
		register_post_type( 'course-class', $args );
	
	}
	add_action( 'init', 'rm_courses_class_cpt', 0 );

}

/*
* Register VR User Content Custom Post Type
*/
if ( ! function_exists('rm_vr_user_content') ) {

	// Register Custom Post Type
	function rm_vr_user_content() {
	
		$labels = array(
			'name'                  => _x( 'VR User Contents', 'Post Type General Name', 'text_domain' ),
			'singular_name'         => _x( 'VR User Content', 'Post Type Singular Name', 'text_domain' ),
			'menu_name'             => __( 'VR User Content', 'text_domain' ),
			'name_admin_bar'        => __( 'VR User Content', 'text_domain' ),
			'archives'              => __( 'VR User Content Archives', 'text_domain' ),
			'attributes'            => __( 'VR User Content Attributes', 'text_domain' ),
			'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
			'all_items'             => __( 'All Items', 'text_domain' ),
			'add_new_item'          => __( 'Add New Item', 'text_domain' ),
			'add_new'               => __( 'Add New', 'text_domain' ),
			'new_item'              => __( 'New Item', 'text_domain' ),
			'edit_item'             => __( 'Edit Item', 'text_domain' ),
			'update_item'           => __( 'Update Item', 'text_domain' ),
			'view_item'             => __( 'View Item', 'text_domain' ),
			'view_items'            => __( 'View Items', 'text_domain' ),
			'search_items'          => __( 'Search Item', 'text_domain' ),
			'not_found'             => __( 'Not found', 'text_domain' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
			'featured_image'        => __( 'Featured Image', 'text_domain' ),
			'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
			'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
			'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
			'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
			'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
			'items_list'            => __( 'Items list', 'text_domain' ),
			'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
			'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
		);
		$args = array(
			'label'                 => __( 'VR User Content', 'text_domain' ),
			'description'           => __( 'User notes, audio and screenshots.', 'text_domain' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'editor', 'author' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 100,
			'show_in_admin_bar'     => false,
			'show_in_nav_menus'     => false,
			'can_export'            => false,
			'has_archive'           => false,
			'exclude_from_search'   => true,
			'publicly_queryable'    => true,
			'capabilities' => array(
				'edit_post' => 'edit_vr_user_content',
				'edit_posts' => 'edit_vr_user_contents',
				'edit_others_posts' => 'edit_other_vr_user_contents',
				'publish_posts' => 'publish_vr_user_contents',
				'read_post' => 'read_vr_user_content',
				'read_private_posts' => 'read_private_vr_user_contents',
				'delete_post' => 'delete_vr_user_content'
			),
		);
		register_post_type( 'vr_user_content', $args );
	
	}
	add_action( 'init', 'rm_vr_user_content', 0 );
	
}

/*
* Adds course and course-classes CPT Custom fields
*/

if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array (
		'key' => 'group_5aa10cad2ca64',
		'title' => 'Class Configuration',
		'fields' => array (
			array (
				'key' => 'class_public_title',
				'label' => 'Public Title',
				'name' => 'class_public_title',
				'type' => 'text',
				'instructions' => 'This name will be shown to users',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5aa10cc05ed7c',
				'label' => 'Course',
				'name' => 'course_class',
				'type' => 'relationship',
				'instructions' => 'Select related Course',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'post_type' => array (
					0 => 'sfwd-courses',
				),
				'taxonomy' => array (
				),
				'filters' => array (
					0 => 'search',
					1 => 'post_type',
					2 => 'taxonomy',
				),
				'elements' => '',
				'min' => 1,
				'max' => 1,
				'return_format' => 'object',
			),
			array (
				'key' => 'field_5aa1213d30ed0',
				'label' => 'Trainer',
				'name' => 'class_trainer',
				'type' => 'user',
				'instructions' => 'Select Trainer for this Class. Only Users of role "Trainer" are shown.',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'role' => array (
					0 => 'trainer',
				),
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_5aa11612bd822',
				'label' => 'Students',
				'name' => 'class_students',
				'type' => 'user',
				'instructions' => 'Please select Course and save this Class before selecting Students. Only Students enrolled to the Course will be shown here.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'role' => '',
				'allow_null' => 0,
				'multiple' => 1,
			),
			array (
				'key' => 'field_5aa1219830ed1',
				'label' => 'Date / Time',
				'name' => 'class_datetime',
				'type' => 'date_time_picker',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'display_format' => 'd/m/Y g:i a',
				'return_format' => 'd/m/Y g:i a',
				'first_day' => 1,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'course-class',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'acf_after_title',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));

	if( function_exists('acf_add_local_field_group') ):

		acf_add_local_field_group(array (
			'key' => 'group_5aaf74dc983b9',
			'title' => 'VR Content',
			'fields' => array (
				array (
					'key' => 'field_5aaf74e4148d9',
					'label' => 'VR content',
					'name' => 'vr_content',
					'type' => 'repeater',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'collapsed' => '',
					'min' => 0,
					'max' => 0,
					'layout' => 'table',
					'button_label' => 'Add VR Content',
					'sub_fields' => array (
						array (
							'key' => 'field_5aaf74fe148da',
							'label' => 'File',
							'name' => 'vr_content_file',
							'type' => 'file',
							'instructions' => '',
							'required' => 1,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'return_format' => 'array',
							'library' => 'all',
							'min_size' => '',
							'max_size' => '',
							'mime_types' => '',
						),
						array (
							'key' => 'field_5aaf7527148db',
							'label' => 'Description',
							'name' => 'vr_content_description',
							'type' => 'text',
							'instructions' => '',
							'required' => 1,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'maxlength' => '',
						),
						array (
							'key' => 'field_5ace17232db30',
							'label' => 'Type',
							'name' => 'type',
							'type' => 'radio',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'choices' => array (
								'Hologram' => 'Hologram',
								'Slideshow' => 'Slideshow',
								'Video' => 'Video',
							),
							'allow_null' => 0,
							'other_choice' => 0,
							'save_other_choice' => 0,
							'default_value' => '',
							'layout' => 'vertical',
							'return_format' => 'value',
						),
					),
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'sfwd-lessons',
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'sfwd-topic',
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'course-class',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));
		
	endif;

	acf_add_local_field_group(array (
		'key' => 'group_5a9d5e9a88910',
		'title' => 'Course Classes',
		'fields' => array (
			array (
				'key' => 'field_5aa004e0132ef',
				'label' => 'Related Classes',
				'name' => 'course_class',
				'type' => 'relationship',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'post_type' => array (
					0 => 'course-class',
				),
				'taxonomy' => array (
				),
				'filters' => array (
					0 => 'search',
					1 => 'post_type',
					2 => 'taxonomy',
				),
				'elements' => '',
				'min' => '',
				'max' => '',
				'return_format' => 'object',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'sfwd-courses',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));
	
endif;

/*
* Adds VR User Content CPT Fields
*/

if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array (
		'key' => 'group_5ac7bf3c80fb5',
		'title' => 'VR User Content fields',
		'fields' => array (
			array (
				'key' => 'field_5ac7bf45d17c8',
				'label' => 'File',
				'name' => 'file',
				'type' => 'file',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'url',
				'library' => 'all',
				'min_size' => '',
				'max_size' => '',
				'mime_types' => '',
			),
			array (
				'key' => 'field_5ac7bf6fd17c9',
				'label' => 'Type',
				'name' => 'vr_content_type',
				'type' => 'radio',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => array (
					'audio' => 'Audio Note',
					'screenshot' => 'Screenshot',
				),
				'allow_null' => 0,
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => '',
				'layout' => 'vertical',
				'return_format' => 'value',
			),
			array (
				'key' => 'field_5ac7bfc5d17ca',
				'label' => 'Description',
				'name' => 'description',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5ace15533edcc',
				'label' => 'Class ID',
				'name' => 'class_id',
				'type' => 'relationship',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'post_type' => array (
					0 => 'course-class',
				),
				'taxonomy' => array (
				),
				'filters' => array (
					0 => 'search',
				),
				'elements' => '',
				'min' => '',
				'max' => '',
				'return_format' => 'object',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'vr_user_content',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));
	
	endif;

/*
* Adds bidirectional relationship between Courses and Classes
*/
function bidirectional_acf_update_value( $value, $post_id, $field  ) {

	// vars
	$field_name = $field['name'];
	$field_key = $field['key'];
	$global_name = 'is_updating_' . $field_name;
	
	// bail early if this filter was triggered from the update_field() function called within the loop below
	// - this prevents an inifinte loop
	if( !empty($GLOBALS[ $global_name ]) ) return $value;
	
	// set global variable to avoid inifite loop
	// - could also remove_filter() then add_filter() again, but this is simpler
	$GLOBALS[ $global_name ] = 1;
	
	// loop over selected posts and add this $post_id
	if( is_array($value) ) {
		foreach( $value as $post_id2 ) {
			// load existing related posts
			$value2 = get_field($field_name, $post_id2, false);

			// allow for selected posts to not contain a value
			if( empty($value2) ) {
				$value2 = array();
			}
			
			// bail early if the current $post_id is already found in selected post's $value2
			if( in_array($post_id, $value2) ) continue;
			
			// append the current $post_id to the selected post's 'related_posts' value
			$value2[] = $post_id;
			
			// update the selected post's value (use field's key for performance)
			update_field($field_key, $value2, $post_id2);			
		}
	}
	
	// find posts which have been removed
	$old_value = get_field($field_name, $post_id, false);
	
	if( is_array($old_value) ) {
		
		foreach( $old_value as $post_id2 ) {
			
			// bail early if this value has not been removed
			if( is_array($value) && in_array($post_id2, $value) ) continue;
			
			// load existing related posts
			$value2 = get_field($field_name, $post_id2, false);
			
			// bail early if no value
			if( empty($value2) ) continue;
			
			// find the position of $post_id within $value2 so we can remove it
			$pos = array_search($post_id, $value2);
			
			// remove
			unset( $value2[ $pos] );
			
			// update the un-selected post's value (use field's key for performance)
			update_field($field_key, $value2, $post_id2);
		}
	}
	
	// reset global varibale to allow this filter to function as per normal
	$GLOBALS[ $global_name ] = 0;
	
	// return
	return $value;
	
}
add_filter('acf/update_value/name=course_class', 'bidirectional_acf_update_value', 10, 3);

/*
* configure capabilities
ref: https://wordpress.stackexchange.com/questions/108338/capabilities-and-custom-post-types
*/

function add_vr_user_content_caps() {
    // gets the subscriber role
    $admins = get_role( 'subscriber' );

    $admins->add_cap( 'edit_vr_user_content' ); 
    $admins->add_cap( 'edit_vr_user_contents' ); 
    $admins->remove_cap( 'edit_other_vr_user_contents' ); 
    $admins->remove_cap( 'publish_vr_user_contents' ); 
    $admins->remove_cap( 'read_vr_user_content' ); 
    $admins->remove_cap( 'read_private_vr_user_contents' ); 
	$admins->remove_cap( 'delete_vr_user_content' ); 
	
	// gets the admin role
	$admins = get_role( 'administrator' );

    $admins->add_cap( 'edit_vr_user_content' ); 
    $admins->add_cap( 'edit_vr_user_contents' ); 
    $admins->add_cap( 'edit_other_vr_user_contents' ); 
    $admins->add_cap( 'publish_vr_user_contents' ); 
    $admins->add_cap( 'read_vr_user_content' ); 
    $admins->add_cap( 'read_private_vr_user_contents' ); 
    $admins->add_cap( 'delete_vr_user_content' ); 
}
add_action( 'admin_init', 'add_vr_user_content_caps');

function th_get_roles_for_post_type( $post_type ) {
    global $wp_roles;

    $roles = array();
    $type  = get_post_type_object( $post_type );

    // Get the post type object caps.
    $caps = array( $type->cap->edit_posts, $type->cap->publish_posts, $type->cap->create_posts );
    $caps = array_unique( $caps );

    // Loop through the available roles.
    foreach ( $wp_roles->roles as $name => $role ) {

        foreach ( $caps as $cap ) {

            // If the role is granted the cap, add it.
            if ( isset( $role['capabilities'][ $cap ] ) && true === $role['capabilities'][ $cap ] ) {
                $roles[] = $name;
                break;
            }
        }
    }

    return $roles;
}

add_action( 'load-post.php',     'th_load_user_dropdown_filter' );
add_action( 'load-post-new.php', 'th_load_user_dropdown_filter' );

function th_load_user_dropdown_filter() {
    $screen = get_current_screen();

    if ( empty( $screen->post_type ) || 'vr_user_content' !== $screen->post_type )
        return;

    add_filter( 'wp_dropdown_users_args', 'th_dropdown_users_args', 10, 2 );
}

function th_dropdown_users_args( $args, $r ) {
    global $wp_roles, $post;

    // Check that this is the correct drop-down.
    if ( 'post_author_override' === $r['name'] && 'vr_user_content' === $post->post_type ) {

        $roles = th_get_roles_for_post_type( $post->post_type );

        // If we have roles, change the args to only get users of those roles.
        if ( $roles ) {
            $args['who']      = '';
            $args['role__in'] = $roles;
        }
    }
	return $args;
}
?>