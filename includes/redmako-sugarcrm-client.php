<?php
require_once('restclient.php');

class RedMako_SugarCrm_Client extends RestClient {
    public function __construct($baseUrl) {
        parent::__construct(array(
            'base_url' => $baseUrl . '/rest/v10'
        ));
    }

    public function response_or_exception($result) {
        if ($result->info->http_code == 200) {
            return $result->decode_response();
        } else {
            throw new \Exception($result->decode_response()->error_message, $result->info->http_code);
        }
    }

    public function authenticate($username, $password) {
        $result = $this->post('oauth2/token', array(
            'grant_type' => 'password',
            'client_id' => 'sugar',
            'client_secret' => '',
            'username' => $username,
            'password' => $password,
            'platform' => 'custom'
        ));

        if($result->info->http_code == 200) {
            $response = $result->decode_response();
            $this->options['headers']['oauth-token'] = $response->access_token;
        }

        return $this->response_or_exception($result);
    }

    public function search_trainees($filter) {
        return $this->response_or_exception($this->get('/RM1_manage_trainee', array(
            'filter' => json_encode($filter)
        )));
    }

    public function get_trainee_by_email($email) {
        $result = $this->search_trainees(array(
            array(
                'email' => $email
            )
        ));
        
        if (count($result->records) > 0) {
            return $result->records[0];
        } else {
            return null;
        }
    }

    public function get_trainee($id) {
        $result = $this->get('/RM1_manage_trainee/' . $id);
        return $this->response_or_exception($result);
    }

    public function search_training_plan($filter) {
        $result = $this->get('/RM4_training_plan', array('filter' => $filter));
        return $this->response_or_exception($result);
    }

    public function get_training_plan_for_trainee($trainee_id) {
        $result = $this->search_training_plan(array(
            array(
                'rm1_manage_trainee_rm4_training_plan_1rm1_manage_trainee_ida' => $trainee_id
            )
        ));
        
        if (count($result->records) > 0) {
            return $result->records[0];
        } else {
            return null;
        }
    }

    public function get_unit_for_training_plan_by_name($training_plan_id, $unit_name) {
        $result = $this->get('/RM4_training_plan/' . $training_plan_id . '/link/rm4_units_rm4_training_plan', array(
            'filter' => array(array(
                'unit' => $unit_name
            ))
        ));

        $result = $this->response_or_exception($result);

        if (count($result->records) > 0) {
            return $result->records[0];
        } else {
            return null;
        }
    }

    public function update_unit($training_plan_id, $id, $update) {
        $result = $this->put('/RM4_training_plan/' . $training_plan_id . '/link/rm4_units_rm4_training_plan/' . $id, json_encode($update));

        return $this->response_or_exception($result);
    }
}