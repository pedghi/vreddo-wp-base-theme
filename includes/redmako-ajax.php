<?php
class RedMako_Ajax {
  public static function callback() {
    $instance = new RedMako_Ajax();

    $func = $_POST['function'];
    $data = $_POST['data'];

    if (!$data) {
      $data = [];
    }

    try
    {
      $func_validation = $func . 'Validation';
      $instance->$func_validation($data);
      $ret = $instance->$func($data);

      wp_send_json(array(
        'success' => true,
        'body' => $ret
      ));
    }
    catch (\Exception $e)
    {
      wp_send_json(array(
        'success' => false,
        'message' => $e->getMessage()
      ));
    }
  }

  public function overrideQuizScoreValidation($data) {
    if (!$data['quiz_id'] || !$data['user_id']) {
      throw new \Exception("Quiz ID and user ID are required.");
    }

    // Check if there's an existing record.
    $stat_mapper = new WpProQuiz_Model_StatisticRefMapper();
    $quiz = $stat_mapper->fetchAll($data['quiz_id'], $data['user_id']);

    if (!$quiz || !count($quiz)) {
      throw new \Exception("User hasn't taken that quiz yet.");
    }
  }

  public function overrideQuizScore($data) {
    $quizId = $data['quiz_id'];
    $userId = $data['user_id'];

    $stat_mapper = new RedMako_WpProQuiz_Model_StatisticRefMapper();
    $quiz = $stat_mapper->fetchAll($data['quiz_id'], $data['user_id']);
    $statRefId = $quiz[count($quiz) - 1]->getStatisticRefId();

    $stats_mapper = new WpProQuiz_Model_StatisticMapper();
    $stats = $stats_mapper->fetchAllByRef($statRefId);

    $question_mapper = new WpProQuiz_Model_QuestionMapper();
    $points = 0;

    foreach ($stats as $stat) {
      $question = $question_mapper->fetch($stat->getQuestionId());

      if ($question) {
        $stat_mapper->overrideStatScore($stat->getStatisticRefId(), $stat->getQuestionId(), $question->getPoints());
      }
    }
    
    // Check if the lesson is complete.
    if (rm_quiz_lesson_complete(learndash_get_quiz_id_by_pro_quiz_id($quizId), $userId)) {
      $lesson = rm_get_lesson_for_quiz(learndash_get_quiz_id_by_pro_quiz_id($quizId), $userId);
      $lesson = explode(' ', $lesson['lesson']->post_title)[0];
      rm_sugarcrm_push_unit_completion($userId, $lesson);
    }

    return array();
  }

  public function postNoteValidation($data) {
    if (!$data['content'] || !$data['location']) {
      throw new \Exception('Content and location are required.');
    }
  }

  public function postNote($data) {
    $location = explode('^', $data['location']);

    $noteid = wp_insert_post(array(
      'post_type' => 'coursenote',
      'post_content' => $data['content'],
      'post_status' => 'publish'
    ));
    
    update_post_meta($noteid, '_nt-course-array', $location);
    
    if (isset($data['for_user']) && $data['for_user'] != wp_get_current_user()->ID && (current_user_can('administrator') || current_user_can('tutor_admin'))) {
      update_post_meta($noteid, '_rm-for-user', $data['for_user']);
    }
    
    return $noteid;
  }

  public function saveNoteValidation($data) {
    if (!$data['content'] || !$data['id']) {
      throw new \Exception('Content and ID are required.');
    }
  }

  public function saveNote($data) {
    $note = get_post($data['id']);

    if (!$note) {
      return "Note doesn't exist.";
    }

    if ($note->post_author == wp_get_current_user()->ID) {
      wp_update_post(array(
        'ID' => $data['id'],
        'post_content' => $data['content']
      ));
    } else {
      throw new \Exception("You can only edit your own notes.");
    }
  }

  public function exportQuizDataValidation($data) {
    if (!$data['user_id']) {
      throw new \Exception('User ID is required.');
    }

    if (!current_user_can('administrator') && !current_user_can('tutor_admin')) {
      throw new \Exception('You are unauthorized.');
    }
  }

  public function exportQuizData($data) {
    $userId = $data['user_id'];
    $user = get_user_by('id', $userId);
  
    // We need to re-query the quiz (posts). This is partly to validate the listing. We don't
    // want to pass old or outdated quiz items to externals. 
    $atts_defaults = array(
      'num' => LearnDash_Settings_Section::get_section_setting( 'LearnDash_Settings_Section_General_Per_Page', 'per_page' ),
      'quiz_orderby' => 'taken',
      'quiz_order' => 'DESC', 
    );

    $usermeta = get_user_meta( $userId, '_sfwd-quizzes', true );
    $quizzes = empty( $usermeta ) ? false : $usermeta;
    
    if ( !empty( $quizzes ) ) {
      
      if ( $atts['quiz_num'] === false )
        $atts['quiz_num'] = intval( $atts_defaults['num'] );
      else
        $atts['quiz_num'] = intval( $atts['quiz_num'] );
    
      if ( ( !isset( $atts['quiz_orderby'] ) ) || ( empty( $atts['quiz_orderby'] ) ) )
        $atts['quiz_orderby'] = $atts_defaults['quiz_orderby'];

      if ( ( !isset( $atts['quiz_order'] ) ) || ( empty( $atts['quiz_order'] ) ) )
        $atts['quiz_order'] = $atts_defaults['quiz_order'];

      if ( !is_null( $atts['quiz_ids'] ) ) {
        $quiz_ids = $atts['quiz_ids'];
      } else {
        $quiz_ids = wp_list_pluck( $quizzes, 'quiz' );
      }

      $quiz_total_query_args = array(
        'post_type'			=>	'sfwd-quiz',
        'fields'			=>	'ids',
        'orderby'			=>	'title', //$atts['quiz_orderby'],
        'order'				=>	'ASC', //$atts['quiz_order'],
        'nopaging'			=>	true,
        'post__in'			=>	$quiz_ids
      );
    
      if ( $quiz_total_query_args['orderby'] == 'taken' ) {
        $quiz_total_query_args['orderby'] = 'title';
      }

      $quiz_query = new WP_Query( $quiz_total_query_args );
      if ( ( $quiz_query ) && ( !is_wp_error( $quiz_query ) ) && is_a( $quiz_query, 'WP_Query' ) ) {
        if ( ( property_exists( $quiz_query, 'posts' ) ) && ( !empty( $quiz_query->posts ) ) ) {
          $quizzes_tmp = array();
          foreach( $quiz_query->posts as $post_idx => $quiz_id ) {
            foreach( $quizzes as $quiz_idx => $quiz_attempt ) {
              if ( $quiz_attempt['quiz'] == $quiz_id ) {
                if ( $atts['quiz_orderby'] == 'taken' ) {
                  $quiz_key = $quiz_attempt['time'] .'-'. $quiz_attempt['quiz']; 
                } else if ( $atts['quiz_orderby'] == 'title' ) {
                  $quiz_key = $post_idx .'-'. $quiz_attempt['time']; 
                } else if ( $atts['quiz_orderby'] == 'ID' ) {
                  $quiz_key = $quiz_attempt['quiz'] .'-'. $quiz_attempt['time'];
                }
                $quizzes_tmp[$quiz_key] = $quiz_attempt;
                unset( $quizzes[$quiz_idx] ); 
              }
            }
          }
          
          $quizzes = $quizzes_tmp;

          if ( $atts['quiz_order'] == 'DESC' ) 
            krsort( $quizzes );
          else
            ksort( $quizzes );
            

          $quizzes_per_page = apply_filters( 'learndash_quiz_info_per_page', $atts['quiz_num'], 'quizzes', $userId );
          if ( $quizzes_per_page > 0 ) {
          
            $quizzes_pager['paged'] = apply_filters('learndash_quiz_info_paged', 1 );
            $quizzes_pager['total_items'] = count( $quizzes );
            $quizzes_pager['total_pages'] = ceil( count( $quizzes ) / $quizzes_per_page );
          
            $quizzes = array_slice ( $quizzes, ( $quizzes_pager['paged'] * $quizzes_per_page ) - $quizzes_per_page, $quizzes_per_page, false );
          }
        }
      }
    }

    $quiz_mapper = new WpProQuiz_Model_QuizMapper();
    $stat_ref_mapper = new RedMako_WpProQuiz_Model_StatisticRefMapper();
    $stat_mapper = new WpProQuiz_Model_StatisticMapper();
    
    foreach ($quizzes as $q) {
      $quiz = $quiz_mapper->fetch($q['pro_quizid']);
      $course = get_post($q['course']);

      if (isset($q['statistic_ref_id']) && $q['statistic_ref_id']) {
        $stat = $stat_ref_mapper->fetchById($q['statistic_ref_id']);

        if ($stat) {
          $q['points'] = $stat->getPoints();
          $q['total_points'] = $stat->getGPoints();
        }
      }
      
      $returned[] = [
        'user_id' => $user->ID,
        'name' => $user->user_login,
        'email' => $user->user_email,
        'quiz_id' => $q['pro_quizid'],
        'quiz_title' => $quiz->getName(),
        'score' => $q['score'],
        'total' => $q['question_show_count'],
        'date' => date(get_option('date_format'), $q['time']),
        'percentage' => round(($q['points'] / $q['total_points']) * 10000) / 100,
        'time_spent' => rm_format_duration($q['timespent']),
        'passed' => $q['points'] == $q['total_points'] ? 'YES' : 'NO',
        'course_id' => $course->ID,
        'course_title' => $course->post_title
      ];
    }

    return $returned;
  }

  public function saveCourseFlowValidation($data) {
    if (!$data['user_id'] || !$data['course_id'] || !$data['course_flow']) {
      throw new \Exception('Missing parameters.');
    }
    
    // Validate course flow.
    try {
      if (!is_array($data['course_flow'])) {
        throw new \Exception();
      }

      foreach ($data['course_flow'] as $lesson) {
        if (!isset($lesson['id']) || !$lesson['id'] || !isset($lesson['children']) || !is_array($lesson['children'])) {
          throw new \Exception();
        }

        // Make sure the lesson ID is actually a lesson.
        $lesson_post = get_post($lesson['id']);
        
        if (!$lesson_post || $lesson_post->post_type != 'sfwd-lessons') {
          throw new \Exception();
        }
        
        foreach ($lesson['children'] as $child) {
          $child_post = get_post($child);
          
          if (!$child_post || ($child_post->post_type != 'sfwd-topic' && $child_post->post_type != 'sfwd-quiz')) {
            throw new \Exception();
          }
        }
      }
    }
    catch (\Exception $e) {
      throw new \Exception('Invalid course flow schema.');
    }
  }

  public function saveCourseFlow($data) {
    update_post_meta($data['course_id'], '_custom_course_flow_' . $data['user_id'], $data['course_flow']);
    
    return true;
  }
}

add_action('wp_ajax_redmako', array('RedMako_Ajax', 'callback'));