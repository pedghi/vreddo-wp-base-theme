<?php
if ($_GET['note']) {
    $note = get_post($_GET['note']);

    if ($note && (current_user_can('administrator') || current_user_can('tutor_admin') || $note->post_author == wp_get_current_user()->ID)) {
        header("Content-type: application/vnd.ms-word");
        $path = get_post_meta( $_GET['note'], '_nt-course-array', true );
        $filename = '';
        $header = '';

        foreach ($path as $step) {
            if ($step) {
                if ($filename) {
                    $filename .= ' - ';
                    $header .= ' > ';
                }

                $filename .= get_post($step)->post_title;
                $header .= get_post($step)->post_title;
            }
        }
        
        // Limit to 100 characters.
        $filename = substr($filename, 0, 100);

        header("Content-Disposition: attachment;Filename=$filename.doc");
        
        echo "<html>";
        echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Windows-1252\">";
        echo "<body>";
        echo '<h1>' . $header . '</h1>';
        echo get_post($_GET['note'])->post_content;
        echo "</body>";
        echo "</html>";
    }
}