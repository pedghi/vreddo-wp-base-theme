<?php
include __DIR__ . '/template/page-start.php';

if (!is_user_logged_in()) {
    auth_redirect();
}

$user = (isset($_GET['user']) && (current_user_can('tutor_admin') || current_user_can('administrator'))) ? get_user_by('login', $_GET['user']) : wp_get_current_user();

if ((current_user_can('tutor_admin') || current_user_can('administrator')) && $user->ID == wp_get_current_user()->ID) {
    header("Location: /my-notes/");
    die();
}

function is_open($loc) {
    $location_hash = '';

    foreach ($loc as $i => $step) {
        if ($i > 0) {
            $location_hash .= '-';
        }

        if ($step != -1) {
            $location_hash .= $step;
        }
    }
    
    return isset($_GET['location']) && $_GET['location'] == $location_hash;
}

function renderNote($note) {
    $currentId = wp_get_current_user()->ID;
    ?>
        <div class="note">
            <?php            
            if ($note->post_author != $currentId) {
                echo $note->post_content;
                ?>
                <?php
            } else {
                wp_editor($note->post_content, 'note-content-' . $note->ID);
                ?>
                <br />
                <button type="button" class="button" style="float: left !important;"
                    data-ajax-click="saveNote"
                    data-ajax-start="tinymce.editors['note-content-<?= $note->ID ?>'].setMode('readonly')"
                    data-ajax-done="tinymce.editors['note-content-<?= $note->ID ?>'].setMode('design')"
                    data-ajax-arg-content="js:tinymce.editors['note-content-<?= $note->ID ?>'].getContent()"
                    data-ajax-arg-id="<?= $note->ID ?>">Save</button>&nbsp;
                <?php
            }
            ?>
            <a href="javascript:printNote(<?=$note->ID?>);" class="button" target="_blank">Print</button>&nbsp;
            <a href="/download-note/?note=<?= $note->ID ?>" class="button" target="_blank">Download</a>
            <div class="note-timestamp">
                <?= date(get_option('date_format'), strtotime($note->post_date)) ?> <?= date(get_option('time_format'), strtotime($note->post_date)) ?>
            </div>
        </div>
    <?php
}

$notes = get_posts(array(
    'posts_per_page' => -1,
    'post_type' => 'coursenote',
    'author' => $user->ID,
    'orderby' => 'post_date',
    'order' => 'DESC'
));

$tutorNotes = get_posts(array(
    'posts_per_page' => -1,
    'post_type' => 'coursenote',
    'meta_key' => '_rm-for-user',
    'meta_value' => $user->ID,
    'orderby' => 'post_date',
    'order' => 'DESC'
));

$noteList = array();

foreach ($notes as $note) {
    $topic = get_post_meta($note->ID, '_nt-course-array', true);
    $topicHash = $topic[0] . '^' . $topic[1] . '^' . $topic[2];

    if (!isset($noteList[$topicHash])) {
        $noteList[$topicHash] = [];
    }
    
    if (!isset($noteList[$topicHash]['student'])) {
        $noteList[$topicHash]['student'] = [];
    }

    $noteList[$topicHash]['student'][] = $note;
}

foreach ($tutorNotes as $note) {
    if ($note->post_author != $user->ID) {
        $topic = get_post_meta($note->ID, '_nt-course-array', true);
        $topicHash = $topic[0] . '^' . $topic[1] . '^' . $topic[2];

        if (!isset($noteList[$topicHash])) {
            $noteList[$topicHash] = [];
        }
        
        if (!isset($noteList[$topicHash]['tutor'])) {
            $noteList[$topicHash]['tutor'] = [];
        }

        $noteList[$topicHash]['tutor'][] = $note;
    }
}
?>
<?php
foreach ($noteList as $location => $ns) {
    $loc = explode('^', $location);

    foreach ($loc as $i => $n) {
        if (!$n) {
            $loc[$i] = -1;
        }
    }

    $id = rand();

?>
    <div class="notes-container">
        <div class="note-header">
            <?= nt_course_breadcrumbs($loc); ?>
            <a class="expand-collapse" href="javascript:;" data-group="<?= $location ?>"
                onclick="jQuery('.group-notes-container[data-group=\'' + jQuery(this).data('group') + '\']').slideToggle(); if (jQuery(this).text() == '+') { jQuery(this).text('-'); } else { jQuery(this).text('+'); }"
            >
                <?= is_open($loc) ? '-':'+' ?>
            </a>
        </div>
        <div class="group-notes-container" data-group="<?= $location ?>" style="<?php if (!is_open($loc)): ?>display: none;<?php endif ?>; padding-top: 12px;">
            <div class="rm-tabs">
                <a class="rm-tab-option selected" href="#student-notes">
                    Student Notes
                </a>
                <a class="rm-tab-option" href="#tutor-notes">
                    Tutor Notes
                </a>
            </div>
            <div id="tutor-notes" class="rm-tab" style="display: none;">
                <?php
                if (wp_get_current_user()->ID != $user->ID) {
                ?>
                <form class="note" method="POST" action="">
                    <h4>New Note:</h4>
                    <?php wp_editor('', 'note-content-' . $id, array('textarea_name' => 'note-content-' . $location)); ?>
                    <br />
                    <button type="button" class="button"
                        data-ajax-click="postNote"
                        data-ajax-start="tinymce.editors['note-content-<?= $id ?>'].setMode('readonly')"
                        data-ajax-done="tinymce.editors['note-content-<?= $id ?>'].setMode('design')"
                        data-ajax-success="
                            $this.parent().parent().find('.<?= $user->ID == wp_get_current_user()->ID ? 'student-notes' : 'tutor-notes' ?>')
                                .prepend(
                                    $('<div />')
                                    .html(tinymce.editors['note-content-<?= $id ?>'].getContent())
                                    .append(
                                        $('<a />')
                                        .addClass('button')
                                        .text('Print')
                                        .attr('target', '_blank')
                                        .attr('href', 'javascript:printNote(' + data + ');')
                                    )
                                    .append(
                                        $('<a />')
                                        .addClass('button')
                                        .text('Download')
                                        .attr('target', '_blank')
                                        .attr('href', '/download-note/?note=' + data)
                                    )
                                    .append('&nbsp;')
                                    .append(
                                        $('<div />')
                                        .addClass('note-timestamp')
                                        .text('Just now')
                                    )
                                    .addClass('note')
                                );
                            tinymce.editors['note-content-<?= $id ?>'].setContent('')"
                        data-ajax-arg-content="js:tinymce.editors['note-content-<?= $id ?>'].getContent()"
                        data-ajax-arg-location="<?= $location ?>"
                        data-ajax-arg-for-user="<?= $user->ID ?>">Save</button>
                </form>
                <?php
                }
                ?>
                <div class="tutor-notes">
                    <?php
                    if (isset($ns['tutor'])) {
                        foreach ($ns['tutor'] as $note) {
                            renderNote($note);
                        }
                    }
                    ?>
                </div>
            </div>
            <div id="student-notes" class="student-notes rm-tab">
                <?php
                if (isset($ns['student'])) {
                    foreach ($ns['student'] as $note) {
                        renderNote($note);
                    }
                }
                ?>
            </div>
        </div>
    </div>
<?php
}
?>
<?php
include __DIR__ . '/template/page-end.php';
?>