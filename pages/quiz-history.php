<?php
$user = get_user_by('login', $_GET['user']);

$statMapper = new RedMako_WpProQuiz_Model_StatisticRefMapper();
$statsMapper = new WpProQuiz_Model_StatisticMapper();
$quizAttempts = $statMapper->getUserQuizAttempts($user->ID);

if (count($quizAttempts)) {
    $lastQuiz = get_post($quizAttempts[0]['quiz_id']);
    $questionMapper = new WpProQuiz_Model_QuestionMapper();
    $lastQuizStatistics = $statsMapper->fetchAllByRef($quizAttempts[0]['last_ref_id']);
}
?>

<h2>Latest Quiz</h2>
<p><b>Title:</b> <a href="<?= get_the_permalink($lastQuiz->ID); ?>"><?= $lastQuiz->post_title; ?></a><p>
<p><b>Score:</b> <?= $quizAttempts[0]['points'] ?>/<?= $quizAttempts[0]['max_points'] ?><p>
<p>
<b>Incorrect Items:</b><br />
<?php
foreach ($lastQuizStatistics as $stat) {
    $question = $questionMapper->fetchById($stat->getQuestionId());
    
    if ($stat->getIncorrectCount() > 0) {
        ?>
        <b><?= $question->getTitle(); ?>:</b> <?= $question->getQuestion(); ?><br />
        <?php
    }
}
?>
</p>

<style>
    .attempts th, .attempts td {
        text-align: center;
    }

    .attempts td:first-child {
        text-align: left;
    }

    .attempts td.detail {
        padding-left: 20px;
    }
</style>
<h2>Previous Quizzes &amp; Attempts</h2>
<table class="attempts">
    <thead>
        <tr>
            <th>Quiz Title</th>
            <th>Score</th>
            <th>Grade</th>
            <th>Number of Attempts</th>
            <th>Time Spent</th>
            <th>Last Attempt Date</th>
            <th>Total Time Spent</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    <?php
    foreach ($quizAttempts as $quiz) {
        $quizPost = get_post($quiz['quiz_id']);

        ?>
        <tr data-row-type="header" data-row-id="<?= $quiz['quiz_pro_id'] ?>" onclick="jQuery('tr[data-row-type=detail][data-row-id=' + jQuery(this).data('row-id') + ']').toggle()">
            <td>
                <a href="<?= get_the_permalink($quizPost->ID); ?>"><?= $quizPost->post_title; ?></a><br />
                <a href="javascript:;">View Incorrect Items</a> | <a href="javascript:;">View Previous Attempts</a>
            </td>
            <td><span class="points"><?= $quiz['points'] ?></span>/<?= $quiz['max_points'] ?></td>
            <td><span class="percentage"><?= round($quiz['percent_correct'] * 100) ?></span>%</td>
            <td><?= $quiz['attempts']; ?></td>
            <td><!-- Time Spent !--></td>
            <td><?= date(get_option('date_format'), $quiz['last_attempt_time']); ?></td>
            <td><!-- Total Time Spent !--></td>
            <td>
                <?php if (floatval($quiz['percent_correct']) < 1): ?>
                <button class="button" type="button"
                    data-ajax-click="overrideQuizScore"
                    data-ajax-success="$this.parent().parent().find('.points').text(data.points); $this.parent().parent().find('.percentage').text('100'); $this.remove();"
                    data-ajax-arg-quiz-id="<?= $quiz['quiz_pro_id']; ?>"
                    data-ajax-arg-user-id="<?= $user->ID; ?>">Mark 100%</button>
                <?php endif ?>
            </td>
        </tr>
        <?php

        $previousAttempts = $statMapper->getPreviousUserQuizAttempts($user->ID, $quiz['quiz_pro_id'], $quiz['last_ref_id']);

        foreach ($previousAttempts as $i => $attempt) {
            $maxScore = 0;
            $points = 0;

            foreach ($statsMapper->fetchAllByRef($attempt->getStatisticRefId()) as $stat) {
                $points += $stat->getPoints();
                $question = $questionMapper->fetch($stat->getQuestionId());
                $maxScore += $question->getPoints();
            }

            ?>
            <tr data-row-type="detail" data-row-id="<?= $quiz['quiz_pro_id'] ?>" style="display: none;">
                <td class="detail"><?= $quizPost->post_title; ?></td>
                <td><?= $points ?>/<?= $maxScore ?></td>
                <td><?= round($points / $maxScore * 100) ?>%</td>
                <td><?= count($previousAttempts) - $i ?></td>
                <td></td>
                <td><?= date(get_option('date_format'), $attempt->getCreateTime()); ?></td>
                <td></td>
                <td></td>
            </tr>
            <?php
        }
    }
    ?>
    </tbody>
</table>