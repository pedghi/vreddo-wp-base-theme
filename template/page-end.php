<?php
$output = ob_get_clean();

add_filter('the_content', function($content) use ($output) {
    return $output;
});

include get_template_directory() . '/page.php';