var STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
var ARGUMENT_NAMES = /([^\s,]+)/g;
function getParamNames(func) {
  var fnStr = func.toString().replace(STRIP_COMMENTS, '');
  var result = fnStr.slice(fnStr.indexOf('(')+1, fnStr.indexOf(')')).match(ARGUMENT_NAMES);
  if(result === null)
     result = [];
  return result;
}

var RedMako_Ajax = function($) {
    this.$ = $;
};

RedMako_Ajax.prototype = {
    execute: function(functionName, data) {
        var $ = this.$;

        return new Promise(function (resolve, reject) {
            $.ajax({
                method: "POST",
                url: ajaxurl,
                data: {
                    action: "redmako",
                    function: functionName,
                    data: data
                },
                success: function(data) {
                    if (data.success) {
                        resolve(data.body);
                    } else {
                        reject(new Error(data.message));
                    }
                },
                error: function(xhr) {
                    reject(new Error(xhr.responseText));
                }
            });
        });
    },
    overrideQuizScore: function(quiz_id, user_id) {
        return this.execute("overrideQuizScore", { quiz_id, user_id })
    },
    postNote: function(content, location, for_user) {
        return this.execute("postNote", { content, location, for_user });
    },
    saveNote: function(id, content) {
        return this.execute("saveNote", { id, content });
    },
    exportQuizData: function(user_id) {
        return this.execute("exportQuizData", { user_id });
    },
    saveCourseFlow: function(user_id, course_id, course_flow) {
        return this.execute("saveCourseFlow", { user_id, course_id, course_flow });
    }
};

var redmakoAjax = new RedMako_Ajax(jQuery);

jQuery(function($) {
    var QUIZ_ATTEMPTS = window.QUIZ_ATTEMPTS || 0;
    var MAX_QUIZ_ATTEMPTS = window.MAX_QUIZ_ATTEMPTS || null;
	
    $(document).on("click", "[data-ajax-click]", function(e) {
        var $this = $(this);
        var functionName = $this.data("ajax-click");
        var args = [];

        getParamNames(redmakoAjax[functionName]).forEach(function(name) {
            var val = $this.data("ajax-arg-" + name.replace(/_/g, "-"));

            if (typeof val == 'string' && val.indexOf("js:") == 0) {
                val = eval(val.substring(3));
            }

            args.push(val);
        });

        var startAction = $this.data("ajax-start");

        if (startAction) {
            eval(startAction);
        }

        redmakoAjax[functionName]
            .apply(redmakoAjax, args)
            .then(function(data) {
                var successAction = $this.data("ajax-success");

                if (successAction) {
                    eval(successAction);
                }
            })
            .catch(function(e) {
                alert("Error: " + e.message);
                console.log(e);
            })
            .then(() => {
                $this.prop("disabled", false);
                var doneAction = $this.data("ajax-done");

                if (doneAction) {
                    eval(doneAction);
                }
            });
        
        $this.prop("disabled", true);
    });
    
    var startRestartButton = $(".wpProQuiz_button[name=restartQuiz], .wpProQuiz_button[name=startQuiz]");
    startRestartButton.before("<div style='text-align: center; font-size: 16px; font-weight: bold;'>Contact your tutor to get help with this quiz.</div>");
    
    if (MAX_QUIZ_ATTEMPTS) {
        var started = false;
        var attemptsText = $("<div />").addClass("attempts-text");

        setInterval(function() {
            $(".wpProQuiz_quiz .wpProQuiz_question_page:hidden").show();

            if (started) {
                if (QUIZ_ATTEMPTS >= MAX_QUIZ_ATTEMPTS) {
                    startRestartButton.remove();
                }
            }

            $(".attempts-text").text("Attempt " + QUIZ_ATTEMPTS + " of " + MAX_QUIZ_ATTEMPTS);
        }, 50);

        startRestartButton.click(function() {
            QUIZ_ATTEMPTS++;
            started = true;
        });

        startRestartButton.after(attemptsText);
        
        if (QUIZ_ATTEMPTS >= MAX_QUIZ_ATTEMPTS) {
            startRestartButton.remove();
        }
    }

    $(".rm-tab-option").click(function(e) {
        var container = $(this).parent().parent();

        container.find(".rm-tab").hide();
        container.find("#" + $(this).attr("href").substring(1)).show();

        $(this).parent().find(".rm-tab-option").removeClass("selected");
        $(this).addClass("selected");
        
        e.preventDefault();
        e.stopPropagation();
    });

    $(".lesson-action[data-action=expand]").click(function() {
        $(this).find("i").toggleClass("fa-chevron-down").toggleClass("fa-chevron-up");
        $(this).parent().find(".lesson-children").slideToggle();
    });
});

function createCsvText(cols, rows) {
    return cols.join(',') + '\n' +
        rows.map(function(row) {
            return cols.map(function(column) {
                return row[column] ? ('"' + row[column].toString().replace(/"/g, '\\"') + '"') : '';
            }).join(',');
        }).join('\n');
}

function downloadCsv(cols, rows, name) {
    var text = createCsvText(cols, rows);

    var link = document.createElement('a');
    link.setAttribute('href', 'data:text/csv;charset=utf8,' + text);
    link.setAttribute('download', 'export_' + name + '_' + Math.floor((new Date()).getTime() / 1000) + '.csv');
    link.click();
}

function getHierarchyData(id) {
    var lessons = [];
    
    $(".hierarchy-" + id).find(".lesson").each(function() {
        var lesson = {
            id: $(this).data("id"),
            children: []
        };

        $(this).find(".lesson-child").each(function() {
            lesson.children.push($(this).data("id"));
        });

        lessons.push(lesson);
    });

    return lessons;
}

function printNote(noteId) {
    window.open("/print-note/?note_id=" + noteId, "Note Printer", "width=800,height=600");
}