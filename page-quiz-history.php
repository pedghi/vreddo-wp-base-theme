<?php
include __DIR__ . '/template/page-start.php';

if (!is_user_logged_in()) {
    auth_redirect();
}

function render_question($question, $answerData = null) {
    if ($answerData) {
        $userAnswers = json_decode($answerData);
    } else {
        $userAnswers = [];
    }

    ?>
    <?= $question->getTitle(); ?>:</b> <?= $question->getQuestion(); ?><br />
    <?php
    switch ($question->getAnswerType()) {
        case 'multiple':
        case 'single':
            ?>
            <ul>
            <?php
            foreach ($question->getAnswerData() as $i => $answer) {
                ?>
                <li <?php if($answer->isCorrect()): ?>style="background-color: lightgreen;"<?php endif ?> <?php if($userAnswers[$i]): ?>style="background-color: pink;"<?php endif ?>><?= $answer->getAnswer() ?></li>
                <?php
            }
            ?>
            </ul>
            <?php
            break;
        case 'matrix_sort_answer':
            ?>
            <table>
                <tbody>
                    <?php
                    foreach ($question->getAnswerData() as $answer) {
                        ?>
                        <tr>
                            <td style="border-right: 1px solid rgba(0, 0, 0, 0.11);"><?= $answer->getAnswer() ?></td>
                            <td style="background-color: lightgreen; width: 33%;"><?= $answer->getSortString() ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <?php
            break;
    }
}

$user = (isset($_GET['user']) && (current_user_can('tutor_admin') || current_user_can('administrator'))) ? get_user_by('login', $_GET['user']) : wp_get_current_user();

$statMapper = new RedMako_WpProQuiz_Model_StatisticRefMapper();
$statsMapper = new WpProQuiz_Model_StatisticMapper();
$quizAttempts = $statMapper->getUserQuizAttempts($user->ID);
$quizMapper = new WpProQuiz_Model_QuizMapper();

if (count($quizAttempts)) {
    $lastQuiz = get_post(learndash_get_quiz_id_by_pro_quiz_id($quizAttempts[0]->getQuizId()));
    $questionMapper = new WpProQuiz_Model_QuestionMapper();
    $lastQuizStatistics = $statsMapper->fetchAllByRef($quizAttempts[0]->getStatisticRefId());
}
?>

<?php if (count($quizAttempts)): ?>
<h2>Latest Quiz</h2>
<p><b>Title:</b> <a href="<?= get_the_permalink($lastQuiz->ID); ?>"><?= $lastQuiz->post_title; ?></a><p>
<p><b>Score:</b> <?= $quizAttempts[0]->getPoints() ?>/<?= $quizAttempts[0]->getGPoints() ?><p>
<p>
<b>Incorrect Items:</b><br />
<?php
foreach ($lastQuizStatistics as $stat) {
    $question = $questionMapper->fetchById($stat->getQuestionId());
    
    if ($stat->getIncorrectCount() > 0 && $question) {
        render_question($question, $stat->getAnswerData());
    }
}
?>
</p>
<?php endif ?>
<button class="button" style="float: right;"
    data-ajax-click="exportQuizData"
    data-ajax-arg-user-id="<?= $user->ID ?>"
    data-ajax-success="downloadCsv(['user_id', 'name', 'email', 'quiz_id', 'quiz_title', 'score', 'total', 'date', 'percentage', 'time_spent', 'passed', 'course_id', 'course_title'], data, <?= htmlentities(json_encode($user->user_login)) ?>)"
    >Export CSV</button>

<style>
    .attempts th, .attempts td {
        text-align: center;
    }

    .attempts td:first-child {
        text-align: left;
    }

    .attempts td.detail {
        padding-left: 20px;
    }
</style>
<h2>Previous Quizzes &amp; Attempts</h2>
<table class="attempts">
    <thead>
        <tr>
            <th>Quiz Title</th>
            <th>Score</th>
            <th>Grade</th>
            <th>Number of Attempts</th>
            <th>Time Spent</th>
            <th>Last Attempt Date</th>
            <th>Total Time Spent</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    <?php
    foreach ($quizAttempts as $quiz) {
        $quizPost = get_post(learndash_get_quiz_id_by_pro_quiz_id($quiz->getQuizId()));
        $previousAttempts = $statMapper->getPreviousUserQuizAttempts($user->ID, $quiz->getQuizId(), $quiz->getStatisticRefId());
        $stats = $statsMapper->fetchAllByRef($quiz->getStatisticRefId());
        
        $totalQuizTime = $statMapper->getQuizAttemptTime($quiz->getStatisticRefId());

        foreach ($previousAttempts as $attempt) {
            $totalQuizTime += $statMapper->getQuizAttemptTime($attempt->getStatisticRefId());
        }
        ?>
        <tr data-row-type="header" data-row-id="<?= $quiz->getQuizId() ?>">
            <td>
                <a href="<?= get_the_permalink($quizPost->ID); ?>"><?= $quizPost->post_title; ?></a><br />
                <a href="javascript:;" onclick="
                        jQuery('tr[data-row-type=detail][data-row-id=' + jQuery(this).parent().parent().data('row-id') + '][data-row-detail-type!=incorrect]').hide();
                        jQuery('tr[data-row-type=detail][data-row-id=' + jQuery(this).parent().parent().data('row-id') + '][data-row-detail-type=incorrect]').toggle();
                        ">View Incorrect Items</a> |
                <a href="javascript:;" onclick="
                        jQuery('tr[data-row-type=detail][data-row-id=' + jQuery(this).parent().parent().data('row-id') + '][data-row-detail-type!=history]').hide();
                        jQuery('tr[data-row-type=detail][data-row-id=' + jQuery(this).parent().parent().data('row-id') + '][data-row-detail-type=history]').toggle();
                    ">View Previous Attempts</a>
            </td>
            <td><span class="points"><?= $quiz->getCorrectCount() ?></span>/<?= ($quiz->getIncorrectCount() + $quiz->getCorrectCount()) ?></td>
            <td><span class="percentage"><?= ($quiz->getGPoints() == 0 ? 100 : round($quiz->getPoints() / $quiz->getGPoints() * 100)) ?></span>%</td>
            <td><?= count($previousAttempts) + 1 ?></td>
            <td><?= rm_format_duration($statMapper->getQuizAttemptTime($quiz->getStatisticRefId())) ?></td>
            <td><?= date(get_option('date_format'), $quiz->getCreateTime()); ?></td>
            <td><?= rm_format_duration($totalQuizTime) ?></td>
            <td>
                <?php if ($quiz->getPoints() < $quiz->getGPoints()): ?>
                <button class="button" type="button"
                    data-ajax-click="overrideQuizScore"
                    data-ajax-success="$this.parent().parent().find('.percentage').text('100'); $this.remove();"
                    data-ajax-arg-quiz-id="<?= $quiz->getQuizId(); ?>"
                    data-ajax-arg-user-id="<?= $user->ID; ?>">Mark 100%</button>
                <?php endif ?>
            </td>
        </tr>
        <?php
        foreach ($stats as $stat) {
            $question = $questionMapper->fetchById($stat->getQuestionId());
            
            if (($stat->getIncorrectCount() > 0 || $stat->getPoints() == 0) && $question) {
                ?>
                <tr data-row-type="detail" data-row-id="<?= $quiz->getQuizId() ?>" style="display: none;" data-row-detail-type="incorrect">
                    <td colspan="8">
                        <?php render_question($question, $stat->getAnswerData()); ?>
                    </td>
                </tr>
                <?php
            }
        }

        $lastQuizTime = $statMapper->getQuizAttemptTime($quiz->getStatisticRefId());

        foreach ($previousAttempts as $i => $attempt) {
            $maxScore = 0;
            $points = 0;
            $totalQuizTime -= $lastQuizTime;
            $quizTime = $statMapper->getQuizAttemptTime($attempt->getStatisticRefId());

            foreach ($statsMapper->fetchAllByRef($attempt->getStatisticRefId()) as $stat) {
                $points += $stat->getPoints();
                $question = $questionMapper->fetch($stat->getQuestionId());
                $maxScore += $question->getPoints();
            }

            ?>
            <tr data-row-type="detail" data-row-id="<?= $quiz->getQuizId() ?>" style="display: none;" data-row-detail-type="history">
                <td class="detail"><?= $quizPost->post_title; ?></td>
                <td><?= $points ?>/<?= $maxScore ?></td>
                <td><?= round($points / $maxScore * 100) ?>%</td>
                <td><?= count($previousAttempts) - $i ?></td>
            <td><?= rm_format_duration($statMapper->getQuizAttemptTime($attempt->getStatisticRefId())) ?></td>
                <td><?= date(get_option('date_format'), $attempt->getCreateTime()); ?></td>
                <td><?= rm_format_duration($totalQuizTime) ?></td>
                <td></td>
            </tr>
            <?php

            $lastQuizTime = $quizTime;
        }
    }
    ?>
    </tbody>
</table>
<?php
include __DIR__ . '/template/page-end.php';
?>