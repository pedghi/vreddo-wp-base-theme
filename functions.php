<?php
/**
 * @package Boss Child Theme
 * The parent theme functions are located at /boss/buddyboss-inc/theme-functions.php
 * Add your own functions in this file.
 */

/**
 * Sets up theme defaults
 *
 * @since Boss Child Theme 1.0.0
 */
function boss_child_theme_setup()
{
  /**
   * Makes child theme available for translation.
   * Translations can be added into the /languages/ directory.
   * Read more at: http://www.buddyboss.com/tutorials/language-translations/
   */

  // Translate text from the PARENT theme.
  load_theme_textdomain( 'boss', get_stylesheet_directory() . '/languages' );

  // Translate text from the CHILD theme only.
  // Change 'boss' instances in all child theme files to 'boss_child_theme'.
  // load_theme_textdomain( 'boss_child_theme', get_stylesheet_directory() . '/languages' );

}
add_action( 'after_setup_theme', 'boss_child_theme_setup' );

/**
 * Enqueues scripts and styles for child theme front-end.
 *
 * @since Boss Child Theme  1.0.0
 */
function boss_child_theme_scripts_styles()
{
  /**
   * Scripts and Styles loaded by the parent theme can be unloaded if needed
   * using wp_deregister_script or wp_deregister_style.
   *
   * See the WordPress Codex for more information about those functions:
   * http://codex.wordpress.org/Function_Reference/wp_deregister_script
   * http://codex.wordpress.org/Function_Reference/wp_deregister_style
   **/

  /*
   * Styles
   */
  wp_enqueue_style( 'boss-child-custom', get_stylesheet_directory_uri().'/css/custom.css' );
}
add_action( 'wp_enqueue_scripts', 'boss_child_theme_scripts_styles', 9999 );


/****************************** CUSTOM FUNCTIONS ******************************/

// Add your own custom functions here
/*
add_action( 'bp_setup_nav', 'redmako_bp_setup_nav' );

function redmako_bp_setup_nav() {
  global $bp;

  bp_core_new_nav_item(array( 
    'name' => __( 'Quiz History', 'redmako' ), 
    'slug' => 'quiz-history',
    'position' => 100,
    'screen_function' => 'redmako_bp_quiz_history',
    'show_for_displayed_user' => true,
    'item_css_id' => 'redmako_bp_quiz_history'
  ));
}
*/
// Autoloader
foreach (glob(__DIR__ . "/includes/*.php") as $filename)
{
    require_once($filename);
}

function rm_bp_add_profile_extra_custom_links() {
  if (current_user_can('tutor_admin') || current_user_can('administrator')) {
    $username = rm_bp_displayed_user_name();

    ?>
      <li><a href="<?= esc_url(get_site_url(null, 'quiz-history/?user=' . $username)); ?>">Quiz History</a></li>
      <li><a href="<?= esc_url(get_site_url(null, 'course-notes/?user=' . $username)); ?>">Course Notes</a></li>
      <li><a href="<?= esc_url(get_site_url(null, 'completed-unit-report/?user=' . $username)); ?>">Completed Unit Report</a></li>
      <li><a href="<?= esc_url(get_site_url(null, 'course-flow/?user=' . $username)); ?>">Course Flow</a></li>
    <?php
  }
}
add_action( 'bp_member_options_nav', 'rm_bp_add_profile_extra_custom_links' );

function redmako_init() {
  wp_enqueue_script('redmako_functions', get_stylesheet_directory_uri() . '/js/functions.js', array('jquery'));
}
add_action('init', 'redmako_init');

function rm_add_quiz_attempts_content($content) {
  global $post;
  
  $attempts = 0;
  $last_test_incorrect = false;
  
  if ($post->post_type == 'sfwd-quiz') {
    $course = rm_get_course_for_quiz($post->ID);
    $max_quiz_attempts = rm_get_max_quiz_attempts($course->ID, $post->ID);
    
    if ($max_quiz_attempts) {
      $quiz_id = get_post_meta($post->ID, 'quiz_pro_id')[0];

      $ref_mapper = new WpProQuiz_Model_StatisticRefMapper();
      $stat_mapper = new WpProQuiz_Model_StatisticMapper();
      $attempts = $ref_mapper->fetchAll($quiz_id, wp_get_current_user()->ID);

      if (count($attempts)) {
        $last_attempt = $attempts[count($attempts) - 1];
        $stats = $stat_mapper->fetchAllByRef($last_attempt->getStatisticRefId());

        foreach ($stats as $stat) {
          if ($stat->getPoints() == 0) {
            $last_test_incorrect = true;
          }
        }
      }


      return '<script>var QUIZ_ATTEMPTS = ' . json_encode(count($attempts)) . '; var LAST_TEST_INCORRECT = ' . json_encode($last_test_incorrect) . '; var MAX_QUIZ_ATTEMPTS = ' . json_encode($max_quiz_attempts) .  ';</script>' . $content;
    }
  }

  return $content;
}
add_filter('the_content', 'rm_add_quiz_attempts_content');

function rm_add_quiz_attempts_classes($classes) {
  global $post;
  $attempts = 0;

  if ($post->post_type == 'sfwd-quiz') {
    $course = rm_get_course_for_quiz($post->ID);
    $max_quiz_attempts = rm_get_max_quiz_attempts($course->ID, $post->ID);
    
    if ($max_quiz_attempts) {
      $quiz_id = get_post_meta($post->ID, 'quiz_pro_id')[0];

      $ref_mapper = new WpProQuiz_Model_StatisticRefMapper();
      $attempts = count($ref_mapper->fetchAll($quiz_id, wp_get_current_user()->ID));
      $classes[] = $attempts >= $max_quiz_attempts ? 'cant-test' : 'can-test';
    }
  }
  
  return $classes;
}
add_filter('body_class', 'rm_add_quiz_attempts_classes');

function rm_ldnt_template_archive_note_listing() {
  return __DIR__ . '/learndash-notes/archive-note-listing.php';
}
add_filter('ldnt_template_archive-note-listing', 'rm_ldnt_template_archive_note_listing');